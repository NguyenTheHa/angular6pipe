import { Component, OnInit, Input } from '@angular/core';

@Component({
  selector: 'app-user-table',
  templateUrl: './user-table.component.html',
  styleUrls: ['./user-table.component.css']
})
export class UserTableComponent implements OnInit {
  @Input('user')
  set user(user: any) {
    this._user = user;
  }
  get user() {
    return this._user;
  }
  constructor() { }
  // tslint:disable-next-line: variable-name
  private _user: any;
  @Input('userName') getUserNameFromAnother: String;
  @Input('phone') getPhoneFromAnother: Number;

  // tslint:disable-next-line: member-ordering
  public idFilter: string;
  // tslint:disable-next-line: member-ordering
  public nameFilter: string;
  public ageFilter: number;
  public sortValue = 1;
  ngOnInit() {
  }
  onHandleSort(value){
    this.sortValue = value;
    console.warn('this.sortValue', this.sortValue)
  }
}
