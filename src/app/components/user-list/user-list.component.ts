import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';

@Component({
  selector: 'app-user-list',
  templateUrl: './user-list.component.html',
  styleUrls: ['./user-list.component.css']
})
export class UserListComponent implements OnInit {
  public listenFromChild: string;
  public phone: number;

  @Input('married') getMarried: boolean; // Khai bao kieu du lieu

  @Output('listenFromChild') onHandleToParent = new EventEmitter<string>();
  @Output('phone') onHandlePhone = new EventEmitter<number>();

  constructor() { }

  ngOnInit() {
  }

  onSend() {
    this.onHandleToParent.emit(this.listenFromChild);
    this.onHandlePhone.emit(this.phone);
  }
}
