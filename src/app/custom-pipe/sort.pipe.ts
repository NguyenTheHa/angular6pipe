import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'sort'
})
export class SortPipe implements PipeTransform {

  transform(value: any, sortNumber: number): any {
    const arrayValue = [];
    value.map(item => {
      arrayValue.push(item.age);
    });
    var result = value.sort((a, b) => {
      if (a.age > b.age) { return sortNumber; } else if (a.age < b.age) { return -sortNumber; } else { return 0; }
    });
    return [...result]
  }
}
