import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'filter'
})
export class FilterPipe implements PipeTransform {

  transform(value: any, ...args: any[]): any {
    console.warn('value', value)
    console.warn('args', args)
    const id = args[0];
    const name = args[1];
    const age = args[2];
    // tslint:disable-next-line: no-unused-expression
    if (!id && !name && !age) {
      return value;
    } else {
      if (id) {
        return value.filter(item => {
          return item.id.toString().indexOf(id) !== -1;
        });
      }
      if (name) {
        return value.filter(item => {
          return item.name.indexOf(name) !== -1;
        });
      }
      if (age) {
        return value.filter(item => {
          return item.age.toString().indexOf(age) !== -1;
        });
      }
    }
  }

}
