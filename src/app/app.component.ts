import { Component } from '@angular/core';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  title = 'Angular Practice';
  public isMarried = true;
  public users: any = [
    {id: 0, name: 'Nguyen Van A', age: 10},
    {id: 1, name: 'Nguyen Dau A', age: 23},
    {id: 2, name: 'Nguyen Tran B', age: 11},
    {id: 3, name: 'Ly Van S', age: 20},
    {id: 4, name: 'Mac Van D', age: 14},
  ];

  public userName: string;
  public phone: number;
  getDataFromChild(value) {
    this.userName = value;
  }
  getNumberPhone(value) {
    this.phone = value;
  }
}
